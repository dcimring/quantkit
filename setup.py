try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Quant tool for making invetment decisions',
    'author': 'Daniel Cimring',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'daniel@blackhatmedia.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['quantkit'],
    'scripts': [],
    'name': 'quantkit'
}

setup(**config)
