'''
(c) 2011, 2012 Georgia Tech Research Corporation
This source code is released under the New BSD license.  Please see
http://wiki.quantsoftware.org/index.php?title=QSTK_License
for license details.

Created on January, 23, 2013

@author: Sourabh Bajaj
@contact: sourabhbajaj@gatech.edu
@summary: Event Profiler Tutorial
'''


import pandas as pd
import numpy as np
import math
import copy
import QSTK.qstkutil.qsdateutil as du
import datetime as dt
import QSTK.qstkutil.DataAccess as da
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkstudy.EventProfiler as ep

"""
Accepts a list of symbols along with start and end date
Returns the Event Matrix which is a pandas Datamatrix
Event matrix has the following structure :
    |IBM |GOOG|XOM |MSFT| GS | JP |
(d1)|nan |nan | 1  |nan |nan | 1  |
(d2)|nan | 1  |nan |nan |nan |nan |
(d3)| 1  |nan | 1  |nan | 1  |nan |
(d4)|nan |  1 |nan | 1  |nan |nan |
...................................
...................................
Also, d1 = start date
nan = no information about any event.
1 = status bit(positively confirms the event occurence)
"""


def find_events(ls_symbols, d_data):
    ''' Finding the event dataframe '''
    
    print "Finding Events"

    df_close = d_data['close'] # we are using adjusted close

    # calculate bollinger values for SPY
    df_market = pd.DataFrame( df_close['SPY'].copy() ) # need to make a copy then convert back to DataFrame
    df_market['MA'] = pd.rolling_mean(df_market['SPY'],20)
    df_market['MStd'] = pd.rolling_std(df_market['SPY'],20)
    df_market['Bollinger'] = (df_market['SPY'] - df_market['MA']) / df_market['MStd'] 
    
    # Creating an empty dataframe
    df_events = copy.deepcopy(df_close)
    df_events = df_events * np.NAN

    # Time stamps for the event range
    ldt_timestamps = df_close.index
    f = open('orders_bollinger.csv','w')

    for s_sym in ls_symbols:
        
        # calculate bollinger bands for each symbol 
        df_close['MA'] = pd.rolling_mean(df_close[s_sym],20)
        df_close['MStd'] = pd.rolling_std(df_close[s_sym],20)
        df_close['Bollinger'] = (df_close[s_sym] - df_close['MA']) / df_close['MStd']
        
        for i in range(1, len(ldt_timestamps)):

            # Event occurs if:
            # Bollinger value for the equity today <= -2.0
            # Bollinger value for the equity yesterday >= -2.0
            # Bollinger value for SPY today >= 1.0
            
            bollinger_yest = df_close['Bollinger'].ix[ldt_timestamps[i - 1]]
            bollinger_today = df_close['Bollinger'].ix[ldt_timestamps[i]]
            bollinger_spy_today = df_market['Bollinger'].ix[ldt_timestamps[i]]
            
            if bollinger_yest >= -2.0 and bollinger_today <= -2.0 and bollinger_spy_today >= 1.4:
                df_events[s_sym].ix[ldt_timestamps[i]] = 1
                # buy 100 shares and sell them days_to_hold days later
                days_to_hold = 5
                buy_date = ldt_timestamps[i]
                f.write("%d,%d,%d,%s,%s,%d\n" % (buy_date.year,buy_date.month,buy_date.day,s_sym,'Buy',100))
                if i + days_to_hold < len(ldt_timestamps):
                    sell_date = ldt_timestamps[i + days_to_hold]
                else:
                    sell_date = ldt_timestamps[-1]
                f.write("%d,%d,%d,%s,%s,%d\n" % (sell_date.year,sell_date.month,sell_date.day,s_sym,'Sell',100))

    f.close()
    return df_events


if __name__ == '__main__':
    dt_start = dt.datetime(2008, 1, 1)
    dt_end = dt.datetime(2009, 12, 31)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))

    dataobj = da.DataAccess('Yahoo')
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    ls_symbols.append('SPY')

    ls_keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ldf_data = dataobj.get_data(ldt_timestamps, ls_symbols, ls_keys)
    d_data = dict(zip(ls_keys, ldf_data))

    for s_key in ls_keys:
        d_data[s_key] = d_data[s_key].fillna(method='ffill')
        d_data[s_key] = d_data[s_key].fillna(method='bfill')
        d_data[s_key] = d_data[s_key].fillna(1.0)

    df_events = find_events(ls_symbols, d_data)
    print "Creating Study"
    ep.eventprofiler(df_events, d_data, i_lookback=20, i_lookforward=20,
                s_filename='MyEventStudy_bollinger.pdf', b_market_neutral=True, b_errorbars=True,
                s_market_sym='SPY')
