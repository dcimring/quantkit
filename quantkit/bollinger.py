import sys
import csv
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import utils as qk

def main(argv):
    symbol = argv[0]
    
    print "Symbol %s" % symbol
    
    start_date = dt.datetime(2010, 1, 1)
    end_date = dt.datetime(2010, 12, 31)
    timestamps = du.getNYSEdays(start_date, end_date, dt.timedelta(hours=16))
    
    data = qk.get_stock_data_closes(timestamps, [symbol])
    data['MA'] = pd.rolling_mean(data[symbol], 20)
    data['MStd'] = pd.rolling_std(data[symbol], 20)
    data['Upper'] = data['MA'] + data['MStd']
    data['Lower'] = data['MA'] - data['MStd']
    data['Bollinger'] = (data[symbol] - data['MA']) / data['MStd']

    #What is the Bollinger value on 2010/6/14 ?
    query_date = dt.datetime(2010, 6, 14, 16, 0, 0)
    print data['Bollinger'][query_date]

    #buy_dates = data[symbol] >= data['Upper']
    #buy_dates = [timestamps[5], timestamps[22], timestamps[50]]
    
    #import ipdb; ipdb.set_trace()
    
    # draw  chart
    plt.clf()
    fig = plt.figure()
    fig.add_subplot(211)
    plt.plot(timestamps, data[symbol], alpha=0.4)
    #plt.plot(timestamps, data['MA'])
    #plt.plot(timestamps, data['Upper'])
    #plt.plot(timestamps, data['Lower'])
    plt.fill_between(timestamps, data['Lower'], data['Upper'], facecolor='gray', alpha=0.3)
    plt.legend([symbol])
    plt.ylabel('Adjusted Close')
    plt.xlabel('Date')
    fig.autofmt_xdate(rotation=45)
    #for i in buy_dates:
    #    ifplt.axvline(x=i,color='green')
    fig.add_subplot(212)
    plt.plot(timestamps, data['Bollinger'])
    plt.fill_between(timestamps, -1, 1, facecolor='gray', alpha=0.3)
    plt.ylabel('Bollinger')
    fig.autofmt_xdate(rotation=45)
    plt.savefig('bollinger.pdf', format='pdf')

    #import ipdb; ipdb.set_trace() 

if __name__ == "__main__":
    main(sys.argv[1:])
    