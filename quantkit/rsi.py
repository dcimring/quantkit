import sys
import csv
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import utils as qk

def main(argv):
    symbol = argv[0]
    
    print "Symbol %s" % symbol
    
    start_date = dt.datetime(2010, 1, 1)
    end_date = dt.datetime(2010, 12, 31)
    timestamps = du.getNYSEdays(start_date, end_date, dt.timedelta(hours=16))
    
    data = qk.get_stock_data_closes(timestamps,[symbol])
    data['rsi'] = qk.rsi(data[symbol])
    
    #import ipdb; ipdb.set_trace()
    
    # draw  chart
    plt.clf()
    fig = plt.figure()
    fig.add_subplot(211)
    plt.plot(timestamps, data[symbol], alpha=0.4)
    #plt.fill_between(timestamps, data['Lower'], data['Upper'], facecolor='gray', alpha=0.3)
    plt.legend([symbol])
    plt.ylabel('Adjusted Close')
    plt.xlabel('Date')
    fig.autofmt_xdate(rotation=45)
    fig.add_subplot(212)
    plt.plot(timestamps,data['rsi'])
    plt.fill_between(timestamps, 30, 70, facecolor='gray', alpha=0.3)
    plt.ylabel('rsi')
    fig.autofmt_xdate(rotation=45)
    plt.savefig('rsi.pdf', format='pdf')

    #import ipdb; ipdb.set_trace()
    

if __name__ == "__main__":
    main(sys.argv[1:])