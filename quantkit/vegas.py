import random

def spin():
    return random.randrange(0,36     + 1)

def win(n):
    if n >= 1 and n <= 18:
        return True
    else:
        return False

wins = 0
tries = 0

for _ in xrange(10000):

    tries += 1
    money = 255
    bet = 1

    while money > 0 and money < 255 + 70:
        #print money, bet
        if bet > money:
            bet = money
        money -= bet
        if win(spin()):
            money += 2 * bet
            bet = 1
        else:
            bet *= 2
    
    if money > 0:
        wins += 1
    #print money
print float(wins) / float(tries)