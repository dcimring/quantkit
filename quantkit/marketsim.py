import sys
import csv
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import utils as qk

def main(argv):
    initial_cash  = int(argv[0])
    orders_file = argv[1]
    values_file = argv[2]
    
    print "Cash %d" % initial_cash
    print "Orders file %s" % orders_file
    print "Values file %s" % values_file
    
    timestamps = []
    symbols = []
    
    for line in csv.reader(open(orders_file,'rU'),delimiter=','):
        year,month,day,symbol,action,qty = line[:6]
        timestamps.append(dt.datetime(int(year),int(month),int(day)))
        symbols.append(symbol)
        
    start_date = min(timestamps)
    end_date = max(timestamps) + dt.timedelta(days=1)
    symbols = np.unique(symbols)
    # can also use this trick uniqueList = list(set(listWithDuplicates)) 
    print start_date, end_date, symbols    
    
    # fetch stock close data for date range and list of symbols
    timestamps = du.getNYSEdays(start_date, end_date, dt.timedelta(hours=16))
    close_prices = qk.get_stock_data_closes(timestamps,symbols)

    # run through orders file and build a trade matrix
    trade_matrix = pd.DataFrame(np.zeros((len(timestamps),len(symbols))),columns=symbols,index=timestamps)
    for line in csv.reader(open(orders_file,'rU'),delimiter=','):
        year,month,day,symbol,action,qty = line[:6]
        qty = int(qty)
        if action == 'Sell':
            qty = - qty
        trade_date = dt.datetime(int(year),int(month),int(day)) + dt.timedelta(hours=16)
        trade_matrix.loc[trade_date,symbol] = trade_matrix.loc[trade_date,symbol] + qty
    
    cash = pd.Series(np.zeros(len(timestamps)),index=timestamps)
    cash[0] = initial_cash
    
    for line in csv.reader(open(orders_file,'rU'),delimiter=','):
        year,month,day,symbol,action,qty = line[:6]
        qty = int(qty)
        if action == 'Sell':
            qty = - qty
        trade_date = dt.datetime(int(year),int(month),int(day)) + dt.timedelta(hours=16)
        cash.loc[trade_date] = cash.loc[trade_date] - qty * close_prices.loc[trade_date,symbol]
    trade_matrix['CASH'] = cash
    holdings_matrix = trade_matrix.cumsum(axis = 0)
    close_prices['CASH'] = 1
    port_value = holdings_matrix.mul(close_prices)
    port_value['SUM'] = port_value.sum(axis = 1)
    
    #writer = csv.writer(open(values_file,"wb"),delimiter=",")
    f = open(values_file,'w')
    for row_index in port_value.index:
        #print row_index
        #print port_value.loc[row_index,'SUM']
        #writer.writerow([row_index,port_value.loc[row_index,'SUM']])
        f.write("%s, %s, %s, %s\n" % (str(row_index.year),str(row_index.month),str(row_index.day),str(port_value.loc[row_index,'SUM'])))
    f.close()
    
    #import ipdb; ipdb.set_trace()

if __name__ == "__main__":
    main(sys.argv[1:])