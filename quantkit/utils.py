# coding: utf-8

import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math
import itertools
import scipy as sp
import scipy.optimize as optimize

def generate_alloc_space(no_stocks, increments, allow_shorts = False):
    weights = []
    limit  = int(1 / increments)
    start = 0
    if allow_shorts:
        start = -limit
    for x in itertools.product(np.arange(start, limit + 1, 1),repeat = no_stocks):
        if np.sum(x) == limit:
            weights.append(np.array(x) / (limit * 1.0) )
    return weights

def get_stock_data_closes(timestamps,symbols):
    """
    Get dataframe of stock closing prices for timestamps and list of
    stocks in symbols 
    """
    data = get_stock_data(timestamps,symbols)
    data = data['close']
    data = data.fillna(method='ffill')
    data = data.fillna(method='bfill')
    return data

def get_stock_data(timestamps,symbols):
    c_dataobj = da.DataAccess('Yahoo')
    keys = ['open', 'high', 'low', 'close', 'volume', 'actual_close']
    ldf_data = c_dataobj.get_data(timestamps, symbols, keys)
    return dict(zip(keys, ldf_data))

def total_alloc_constraint(x,*args):
    return 1.0 - np.sum(x)
                 
def f(x,*args):
    """
    Function to calculate the sharpe ratio given an array x of share allocations
    and a numpy array args of closing prices for each share
    """
    stock_data = args
    weights = x
    na_port_close = np.sum(stock_data * weights, axis=1) # multiply by portfolio weights
    na_port_rets = na_port_close.copy() # calculate daily returns
    tsu.returnize0(na_port_rets) 
    avg_daily_ret = np.mean(na_port_rets)
    std_daily_ret = np.std(na_port_rets)
    sharpe = math.sqrt(252) * avg_daily_ret / std_daily_ret
    #import ipdb; ipdb.set_trace()
    #print sharpe
    return sharpe * -1.0 # we want to maximise sharpe so minimise -1 * sharpe    
    
def gradient_ascent(start, end, symbols, allow_shorts = False):
    """
    Find the maximum sharpe ratio for the list sybmlos of share codes for 
    the time period starting at datetime start and ending at datetime end
    """
    ls_port_syms = symbols
    dt_start = start
    dt_end = end
    dt_timeofday = dt.timedelta(hours=16)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
    # no_trading_days = len(ldt_timestamps) 
    no_trading_days = 252
    d_data = get_stock_data(ldt_timestamps,ls_port_syms)
    df_comp_close = d_data['close'].copy() # get adjusted closing prices
    df_comp_close = df_comp_close.fillna(method='ffill')
    df_comp_close = df_comp_close.fillna(method='bfill')
    na_comp_close = df_comp_close.values
    na_comp_close = na_comp_close / na_comp_close[0,:] # normalise
    #alloc = generate_alloc_space(len(symbols),0.1)
    sym_count = len(symbols)
    # start with equal allocation to each share, and constraint that each alloc must be >= 0
    #return optimize.minimize(f,[0.25,0.25,0.25,0.25],args=(na_comp_close))
    if allow_shorts:
        bnds = ((-1,1),) * sym_count
    else:
        bnds = ((0,1),) * sym_count
    a,b,c = optimize.fmin_l_bfgs_b(f, np.ones(sym_count) / sym_count, approx_grad=1,bounds=bnds,args=(na_comp_close),factr=10.0)
    cons = ({'type': 'eq', 'fun': lambda x:  sum(x)-1})
    #return optimize.minimize(f, np.ones(sym_count) / sym_count, method='SLSQP', bounds=((0,1),) * sym_count, constraints=cons, args=(na_comp_close),jac=False)
    a = a / sum(a) # scale allocs so that they add up to 1
    return a,b,c
    #return optimize.fmin_slsqp(f,np.array([0.5,0.5,0.0,0.0]),eqcons=[con1,],ieqcons=[con2,con3,con4,con5],args=(na_comp_close))

def brute_force(start, end, symbols, alloc=[], allow_shorts = False):
    """
    Simulate a protfolio of shares. If alloc is not provided then loop through all possible 
    portfolio allocations and return the one that maximises the sharpe ratio.
    """
    ls_port_syms = symbols
    #lf_port_alloc = alloc
    dt_start = start
    dt_end = end
    dt_timeofday = dt.timedelta(hours=16)

    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt_timeofday)
    # no_trading_days = len(ldt_timestamps) 
    no_trading_days = 252
    # print "There have been %d trading days" % no_trading_days
    
    d_data = get_stock_data(ldt_timestamps,ls_port_syms)
    #import ipdb; ipdb.set_trace()
    df_comp_close = d_data['close'].copy() # get adjusted closing prices
    df_comp_close = df_comp_close.fillna(method='ffill')
    df_comp_close = df_comp_close.fillna(method='bfill')
    na_comp_close = df_comp_close.values
    na_comp_close = na_comp_close / na_comp_close[0,:] # normalise
    
    if alloc == []:
        alloc = generate_alloc_space(len(symbols),0.1,allow_shorts)
        #alloc = alloc[:10]
    
    max_sharpe = 0
    # sharpe_array = []
    
    for lf_port_alloc in alloc:
        na_port_close = np.sum(na_comp_close * lf_port_alloc, axis=1) # multiply by portfolio weights
        na_port_rets = na_port_close.copy() # calculate daily returns
        tsu.returnize0(na_port_rets) 
        
        # calculate return metrics
        avg_daily_ret = np.mean(na_port_rets)
        std_daily_ret = np.std(na_port_rets)
        sharpe = math.sqrt(no_trading_days) * avg_daily_ret / std_daily_ret
        cum_ret = na_port_close[-1]
        # sharpe_array.append(sharpe)
        
        # keep track of max
        if sharpe > max_sharpe:
            max_sharpe = sharpe
            ret = (lf_port_alloc, std_daily_ret,avg_daily_ret,sharpe,cum_ret)

    d_data = get_stock_data(ldt_timestamps,['SPY'])
    df_spy_close = d_data['close'].copy() # get adjusted closing prices
    df_spy_close = df_spy_close.fillna(method='ffill')
    df_spy_close = df_spy_close.fillna(method='bfill')
    na_spy_close = df_spy_close.values
    na_spy_close = na_spy_close / na_spy_close[0,:] # normalise
    
    # plt.plot(sharpe_array)
    
    plt.clf()
    fig = plt.figure()
    fig.add_subplot(111)
    plt.plot(ldt_timestamps, na_spy_close, alpha=0.4)
    plt.plot(ldt_timestamps, na_port_close)
    ls_names = ['SPY','Portfolio']
    plt.legend(ls_names)
    plt.ylabel('Cumulative Returns')
    plt.xlabel('Date')
    fig.autofmt_xdate(rotation=45)
    plt.savefig('homework1.pdf', format='pdf')
    
    return ret
    
def pick_n_best_from_sp(n):
    """
    From all S&P stocks find the portfolio of n shares that has the highest
    Sharpe ratio
    """
    # get list of S&P shares
    dt_start = dt.datetime(2012, 1, 1)
    dt_end = dt.datetime(2012, 12, 31)
    ldt_timestamps = du.getNYSEdays(dt_start, dt_end, dt.timedelta(hours=16))
    dataobj = da.DataAccess('Yahoo')
    ls_symbols = dataobj.get_symbols_from_list('sp5002012')
    d_data = get_stock_data(ldt_timestamps,ls_symbols)
    df_comp_close = d_data['close'].copy() # get adjusted closing prices
    df_comp_close = df_comp_close.fillna(method='ffill')
    df_comp_close = df_comp_close.fillna(method='bfill')
    portfolio = [] # start with empty portfolio
    while len(portfolio) < n:
        # calculate sharpe ratio for portfolio plus each remaining S&P share and choose share that results in highest sharpe
        max_sharpe = -100
        sym_max = '';
        for sym in ls_symbols:
            curr_sharpe = calc_sharpe(portfolio + [sym],df_comp_close)
            #print curr_sharpe
            if curr_sharpe > max_sharpe:
                max_sharpe = curr_sharpe
                sym_max = sym
                
        # add chosen share to portfolio and remove from list of remaining shares
        portfolio.append(sym_max)
        ls_symbols.remove(sym)
        print max_sharpe
    return portfolio

def rsi(price, n=14):
    ''' rsi indicator '''
    gain = (price-price.shift(1)).fillna(0) # calculate price gain with previous day, first row nan is filled with 0

    def rsiCalc(p):
        # subfunction for calculating rsi for one lookback period
        avgGain = p[p>0].sum()/n
        avgLoss = -p[p<0].sum()/n 
        rs = avgGain/avgLoss
        return 100 - 100/(1+rs)

    # run for all periods with rolling_apply
    return pd.rolling_apply(gain,n,rsiCalc) 

def calc_performance_metrics(df):
    #ret = []
    df = df.pct_change(axis = 0)
    df.ix[0] = 0 # change NA to 0
    no_trading_days = df.shape[0] 
    no_trading_days = 252
    ret = pd.DataFrame(df.mean(),columns=['Mean'])
    ret['Std'] = df.std(ddof=0)
    ret['Sharpe'] = math.sqrt(no_trading_days) * ret['Mean'] / ret['Std']
    ret['Cum'] = (df + 1).cumprod(axis=0).ix[-1]
    return ret.transpose()
        
def calc_sharpe(symbols,df_comp_close):
    """
    Calculate the sharpe ratio for the list of symbols using close data
    provided in na_close
    """
    # issue - we need the allocs as well
    # simple start point would be to assume equal allocs
    # and then later improve so that it finds the optimim allocs
    
    # remove data for shares not in symbols
    #df_comp_close_symbols = df_comp_close[symbols].copy()
    na_comp_close = df_comp_close[symbols].values
    na_comp_close = na_comp_close / na_comp_close[0,:] # normalise
    #import ipdb; ipdb.set_trace()
    
    lf_port_alloc = np.ones(len(symbols)) / len(symbols)
    na_port_close = np.sum(na_comp_close * lf_port_alloc, axis=1) # multiply by portfolio weights
    na_port_rets = na_port_close.copy() # calculate daily returns
    tsu.returnize0(na_port_rets) 
    avg_daily_ret = np.mean(na_port_rets)
    std_daily_ret = np.std(na_port_rets)
    no_trading_days = na_port_close.shape[0] # use number of rows
    #print no_trading_days
    sharpe = math.sqrt(no_trading_days) * avg_daily_ret / std_daily_ret
    return sharpe
    

    
        
    

