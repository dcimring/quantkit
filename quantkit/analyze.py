import sys
import csv
import QSTK.qstkutil.qsdateutil as du
import QSTK.qstkutil.tsutil as tsu
import QSTK.qstkutil.DataAccess as da
import datetime as dt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import utils as qk

def main(argv):
    values_file = argv[0]
    comp_symbol = argv[1]
    
    print "Values file %s" % values_file
    print "Comp symbol %s" % comp_symbol
    
    timestamps = []
    port_values = []
    
    for line in csv.reader(open(values_file,'rU'),delimiter=','):
        year,month,day,val = line[:4]
        val = float(val)
        timestamps.append(dt.datetime(int(year),int(month),int(day),16,0))
        port_values.append(val)
    
    portfolio = pd.DataFrame(data=port_values, index=timestamps,columns=['Portfolio'])    
    portfolio[comp_symbol] = qk.get_stock_data_closes(timestamps,[comp_symbol])
    portfolio = portfolio.div(portfolio.ix[0])
    print qk.calc_performance_metrics(portfolio)
    
    portfolio.plot()
    
    # draw comparison chart
    plt.clf()
    fig = plt.figure()
    fig.add_subplot(111)
    plt.plot(timestamps, portfolio['Portfolio'], alpha=0.4)
    plt.plot(timestamps, portfolio[comp_symbol])
    plt.legend(portfolio.columns)
    plt.ylabel('Cumulative Returns')
    plt.xlabel('Date')
    fig.autofmt_xdate(rotation=45)
    plt.savefig('homework3.pdf', format='pdf')
    
    #import ipdb; ipdb.set_trace()
    

if __name__ == "__main__":
    main(sys.argv[1:])