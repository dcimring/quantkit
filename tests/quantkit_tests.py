from nose.tools import *
import quantkit.utils as qk
import datetime as dt

def setup():
    print "SETUP!"

def teardown():
    print "TEAR DOWN!"

def test_basic():
    print "I RAN!"
    
def test_simulate():
    res = qk.simulate(dt.datetime(2010, 1, 1),dt.datetime(2010, 12, 31),['BRCM', 'TXN', 'IBM', 'HNZ'],[])
    assert_equal(res,([0.1, 0.1, 0.0, 0.8], 0.0096648395624620662, 0.0008433129857150129, 1.3851423543082, 1.2212445353966501))
    res = qk.simulate(dt.datetime(2010, 1, 1),dt.datetime(2010, 12, 31),['AAPL','GOOG','IBM','MSFT'] ,[])
    assert_equal(res,([1.0, 0.0, 0.0, 0.0], 0.016831594336161416, 0.0017905981418011325, 1.6887802616827754, 1.512341623650868))
    
def test_generate_alloc_space():
    res = qk.generate_alloc_space(2,.5)
    assert_equal(res,[[0.0, 1.0], [0.5, 0.5], [1.0, 0.0]])
    res = qk.generate_alloc_space(2,.5,False)
    assert_equal(res,[[0.0, 1.0], [0.5, 0.5], [1.0, 0.0]])
    res = qk.generate_alloc_space(2,.5,True)
    assert_equal(res,[[-0.5, -0.5], [-0.5, 0.5], [0.5, -0.5], [0.5, 0.5]])
    